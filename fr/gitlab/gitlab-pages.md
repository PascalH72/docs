# Comment utiliser les Gitlab Pages

Les Gitlab Pages permettent à qui possède un compte sur <https://framagit.org> (qui utilise [Gitlab][1]) de publier un site statique sur notre infrastructure, soit en sous-domaine de frama.io (par exemple : <https://luc.frama.io>) soit avec un domaine rien qu'à vous.

Les Gitlab Pages remplacent (avantageusement) [FsPages][2] que nous proposions auparavant.

Tout d'abord, voici le lien de la documentation officielle des Gitlab Pages : <https://docs.gitlab.com/ce/user/project/pages/index.html>.

## Comment ça marche ?

Les Gitlab Pages utilisent Gitlab CI, le système d'intégration continue de Gitlab, pour générer des sites statiques à l'aide de générateurs tels [Hugo][3] ou [Pelican][4] (vous pouvez en utiliser bien d'autres ou aucun).

À chaque *push*, Gitlab CI va démarrer un conteneur docker (que vous aurez préalablement choisi), exécuter un script (que vous aurez écrit) et récupérera un dossier `public` pour le publier.

## Avantages par rapport à FsPages

Premièrement, vous n'avez nul besoin d'installer un générateur de site statique sur votre ordinateur pour publier via les Gitlab Pages, contrairement à FsPages (à moins que vous ne fassiez tout à la main). À noter cependant que la plupart des générateurs possèdent une commande vous permettant de créer facilement l'arborescence idoine. Mais une fois le système mis en place, vous n'aurez plus besoin du générateur 🙂

Vous n'aurez même plus besoin de rien puisque vous pourrez modifier directement vos fichiers sources dans l'interface web de Gitlab et laisser Gitlab Pages régénérer votre site (mais bon, ça, ça nécessite que vous ayez déjà mis en place tout le système).

Deuxièmement, vous pouvez utiliser le nom de domaine que vous voulez ! Vous n'êtes plus obligés de vous contenter de https://votre_user.frama.io. Et vous pourrez aussi déposer un certificat afin que votre site avec votre nom de domaine personnel soit joignable en HTTPS.

## Exemple d'un site généré avec Hugo

### Prise en main d'Hugo

Pour plus de détails, rendez-vous sur <https://gohugo.io/overview/quickstart/>

*   installez Hugo sur votre ordinateur : rendez-vous sur <https://github.com/spf13/hugo/releases>, téléchargez l'installeur correspondant à votre système d'exploitation et lancez-le
*   si vous avez déjà un dépôt git, rendez-vous dans son dossier, sinon créez-en un sur <https://framagit.org/projects/new>, clonez-le et allez dans son dossier
*   créez un nouveau site avec Hugo : `hugo new site mon_site; cd mon_site`
*   éditez le fichier `config.toml`
*   ajoutez un [thème][5] dans le dossier `themes`
*   créez du contenu pour votre site : `hugo new post/toto.md`
*   éditez `content/post/toto.md`
*   regardez ce que ça donne en lançant `hugo server --theme=hugo_theme_robust --buildDrafts` puis en vous rendant sur <http://localhost:1313/>
*   si vous êtes satisfait du résultat, éditez `content/post/toto.md` et passez `draft à false`
*   pour éviter de devoir passer l'argument `--theme=hugo_theme_robust` à hugo à chaque fois, ajoutez `theme = "hugo_theme_robust"` à votre fichier `config.toml`

### Publication via les Gitlab Pages

Pour plus de détails, rendez-vous sur <https://gohugo.io/tutorials/hosting-on-gitlab/>

```
echo "/public/" >> .gitignore
vi .gitlab-ci.yml
```

*Concernant l'intégration continue* :

* si vous n'avez pas encore de fichier `.gitlab-ci.yml`, vous pouvez choisir un modèle en vous rendant dans `Set up CI` et en choisissant le template `Hugo`,
* ou bien ajoutez ceci à votre fichier `.gitlab-ci.yml` (attention ! Si vous avez déjà de l'intégration continue en place, il peut être opportun soit d'utiliser votre image classique et d'y installer hugo (comme sur, par exemple, <https://gitlab.com/pages/hugo>) soit de faire une branche dédiée).

```
image: monachus/hugo

pages:
  script:
    - hugo
  artifacts:
    paths:
      - public
  only:
    - master
```

Puis :

```
git add --all
git commit -m "Publication de mon super site"
git push
```

Et voilà ! Votre site devrait être accessible à l'adresse https://votre_user.frama.io/votre_depot ou directement à https://votre_user.frama.io/ si votre dépôt s'appelle votre_user.frama.io.

Rendez-vous sur <https://framagit.org/votre_user/votre_depot/pages> si vous souhaitez que votre site soit accessible avec le nom de domaine de votre choix.

### Passer de FsPages aux Gitlab Pages

Si vous utilisiez Fspages, il est très simple d'adapter votre dépôt git pour qu'il utilise désormais Gitlab Pages. En effet, FsPages ne permettait que de publier des dossiers contenant des pages HTML déjà préparées, et les Gitlab Pages permettent de faire la même chose.

*   supprimez le webhook qui servait pour FsPages (sur <https://framagit.org/votre_user/votre_projet/settings/integrations>), il s'agit du webhook à l'adresse semblable à http://127.0.0.1:4246
*   copiez le fichier .gitlab-ci.yml de <https://gitlab.com/pages/plain-html> dans votre dépôt git
*   modifiez ce fichier :
    *   si le dossier à publier ne s'appelle pas `public`, à la place de `echo 'Nothing to do...'`, mettez `mv votre_dossier public`
    *   au lieu de `master` tout en bas, indiquez `fs-pages` (sauf si vous voulez vous débarrasser de la branche fs-pages, auquel cas il vous faut copier le contenu de la branche fs-pages dans la branche master)
*   supprimez le fichier `.fs-pages.json`
*   commitez et pushez 🙂

Si vous souhaitez que votre dépôt git fournisse la racine de votre site (i.e. qu'il ne soit pas publié dans un sous-dossier de https://votre_user.frama.io), il suffit de renommer votre dépôt en votre_user.frama.io (exemple : <https://framagit.org/luc/luc.frama.io>) via la page https://framagit.org/votre_user/votre_projet/edit

## Utiliser un domaine personnalisé accessible en https

Framasoft se charge de créer le certificat [Let's Encrypt][7] pour vous.

Dans cet exemple, nous allons lier le domaine `docs.gaspacho.space` à la page gitlab `framagit/ddd/gaspacho`.

### Changer le domaine du dépôt

À partir d'ici vous devriez avoir une page web accessible depuis `votre-user.frama.io/votre-depot` (ou bien `votre-organisation.frama.io/votre-depot`).

Pour accéder à cette page depuis `votre-user.frama.io`, allez dans **Paramètres** / **Général**, puis **Advanced**, **Rename repository**, remplacez le *Path* actuel par `votre-user.frama.io`.

![](images/gitlab-change-path.png)

Essayez maintenant d'accéder à votre dépôt via https://votre-user.frama.io (ici, le chemin est donc `ddd.frama.io`).

### Ajouter un domaine

Allez dans **Paramètres** / **Pages** et cliquez sur le bouton **New Domain** en haut à droite, puis remplissez seulement le champ **Domain** et laissez le reste vide pour l'instant, puis cliquez sur **Create New Domain**.

![](images/gitlab-new-domain.png)

### Configuration DNS

Gitlab vous donne alors des entrées DNS (qu'on peut aussi obtenir depuis la page **Paramètres**, **Pages**, puis en dessous du domaine précédemment créé, cliquez sur le bouton `Détail`).

![](images/gitlab-domain-details.png)

Entrez ces entrées DNS via l'interface de votre *registar*. Sur OVH, c'est par exemple (sur la nouvelle interface) : **Domaine**, **[votre domaine]**, onglet **Zone DNS**, bouton **Ajouter une entrée**.

![](images/gitlab-new-dns-entry.png)

Ajoutez l'entrée `CNAME`, en rentrant le sous-domaine et la cible, tels qu'indiqués par gitlab (ne pas oublier le `.` à la fin du domaine cible).

![](images/gitlab-dns-entry-cname.png)

De la même manière, ajoutez l'entrée `TXT` avec le sous-domaine (attention OVH rajoute automatiquement le domaine principal à la fin) et la valeur, tels qu'indiqués par gitlab.

![](images/gitlab-dns-entry-txt.png)

Revenez sur Gitlab, toujours dans la page d'édition du domaine, cliquez sur le bouton **Vérifier**. Il devrait se mettre à jour en vert.

![](images/gitlab-verified.png)

Essayez maintenant d'accéder à votre dépôt via http://votre-user.frama.io. Vous devriez maintenant avoir un avertissement vous indiquant que le domaine n'est pas vérifié (et non une page 404). On avance.

![](images/gitlab-url-http.png)

### Génération du certificat avec Let's Encrypt

<p class="alert alert-warning">Si l'accès à votre dépôt est limité aux membres, <b>la génération du certificat ne pourra être faite</b>&nbsp;!</p>

Aucune action de votre part n'est requise à ce niveau : un script passe toutes les heures à xxh38 pour générer les certificats. Vous n'aurez donc qu'une heure maximum à attendre pour en avoir un.

<p class="alert alert-warning">Il faut qu'un site soit déployé (même une page vide) : sans site déployé sur gitlab pages, le script ne peut créer un certificat.</p>

 [1]: https://framacloud.org/cultiver-son-jardin/installation-de-gitlab-et-mattermost/
 [2]: https://framagit.org/luc/fs-pages
 [3]: https://gohugo.io/
 [4]: https://blog.getpelican.com/
 [5]: https://themes.gohugo.io/
 [7]: https://letsencrypt.org/
