# Comprendre Loomio

Vous pouvez accéder aux parties les plus importantes de l'application Loomio via la barre latérale, le menu de navigation principal qui se trouve sur le côté gauche de la page. La barre latérale est ouverte par défaut sur les grands écrans et réduite par défaut sur les petits écrans (comme les smartphones). Il peut être basculé via l'icône <i class="fa fa-bars" aria-hidden="true"></i> dans la barre de navigation.

<img class="gif" alt="animation du menu latéral" src="images/sidebar-en.gif" />

## Naviguer dans vos fils de discussions

### Fils récents

**Fils récents** est la page d‎‎’accueil quand vous vous connectez à Loomio. Elle vous donne un aperçu des activités les plus récentes dans vos groupes. La page **Fils récents** affiche le contenu depuis vos groupes selon une liste priorisée. Les propositions ouvertes sont en haut, suivies par les fils de discussions **Favoris** (marqués d‎‎’une étoile), puis tous les autres fils de discussions de votre groupe, classées par activité la plus récente&nbsp;: **Aujourd‎‎’hui**, **Hier**, **Ce mois-ci**, **Plus d‎‎’un mois**.

Si un fil ou le titre d‎‎’une proposition est **en gras**, alors il y a eu de l‎‎’activité depuis votre dernière visite. Les nombres entre parenthèses après les titres indiquent combien de nouveaux commentaires ou votes ont été postés depuis votre dernière visite. Les propositions vont montrer un camembert, avec votre position en surimpression sur le diagramme. Si vous n‎‎’avez pas pris position, il y aura un point d‎‎’exclamation.

### Fils favoris

Cliquez sur l‎‎’icône étoile dans l‎‎’aperçu d‎‎’un fil pour ajouter ce fil aux **favoris**. Utilisez les étoiles pour distinguer les fils importants. Vous êtes la seule personne qui puisse voir si vous avez ajouté un fil à vos favoris.

### Fils non lus

Cette page montre tous les fils et propositions dans lesquels du contenu a été ajouté depuis votre dernière visite, classés par groupe. Vous pouvez cliquer sur **Marquer comme lu** sur l‎‎’aperçu d‎‎’un fil pour l‎‎’enlever de cette page sans aller voir le fil. Vous pouvez aussi [désactiver](keeping_up_to_date.html#thread-volume) les notifications d‎‎’un fil ici.

### Ignorés

La page **Discussions ignorées** affiche tous les fils que vous avez précédemment ignoré. Cette possibilité est disponible via l'option **Ignorer** de chaque fil. Les fils ignorés n'apparaissent pas dans la page **Discussions récentes** ni dans la page **Discussions non lues**. Vous pouvez ignorer une discussion en passant la souris au-dessus de celui-ci dans les pages **Discussions récentes** ou **Discussions non lues** et en cliquant sur l'icône **Ignorer**.

### Filtrage des fils

Le menu déroulant **Filtres** vous permet de sélectionner ce que vous voyez&nbsp;:

* **Fils récents&nbsp;:** tous les fils et propositions avec de l‎‎’activité récente.

* **Fils contribués&nbsp;:** seulement les fils de discussion et propositions auxquels vous avez participé.

* **Silencieux&nbsp;:** seulement les fils dont vous avez [désactiver](keeping_up_to_date.html#thread-volume) les notifications.

## Visualiser vos groupes

Tous les groupes auxquels vous appartenez sont listés dans la barre latérale. Si vous n'êtes membre que d'un seul groupe Framavox, la page d'accueil de ce groupe sera votre page d'accueil lorsque vous vous connecterez à Framavox. Si vous avez plusieurs groupes, vous pouvez facilement naviguer entre eux en utilisant la barre latérale.

## Explorer les groupes publics

Vous pouvez explorer, et demander à être membre de groupes publics en visitant la page [Groupes publics](https://framavox.org/explore) (dans la barre latérale gauche).

## Modifier vos informations

Vous pouvez voir et mettre à jour vos données de profil via **Modifier mon profil** dans le menu (en cliquant sur votre avatar). Vous pouvez en savoir plus sur la page [Votre profil](your_user_profile.html).

Vous pouvez voir et mettre à jour votre adresse mail via **Paramètres e-mail** dans le menu (en cliquant sur votre avatar). Vous pouvez en savoir plus sur la page [Rester à jour](keeping_up_to_date.html).
