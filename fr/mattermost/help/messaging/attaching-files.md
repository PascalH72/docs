# Joindre des fichiers

_____

## Méthodes pour joindre un fichier

Joignez un fichier en effectuant un glisser-déposer, ou cliquez sur l’icône pièce jointe dans la fenêtre de création de message.

#### Glisser-Déposer

Téléversez un fichier ou une sélection de fichiers en les glissant-déposant depuis votre ordinateur vers la barre latérale droite ou le panneau central. Le glisser-déposer joint les fichiers à la fenêtre de composition de messages, et vous pouvez éventuellement ajouter un message avant d’appuyer sur **ENTRÉE** pour envoyer.


#### L'icône pièce jointe.

Vous pouvez également téléverser des fichiers en cliquant sur l'icône grise en forme de trombone dans la fenêtre de composition de messages. Ceci ouvrira votre gestionnaire de fichiers, d'où vous pourrez accéder aux fichiers à envoyer, puis cliquer sur **Ouvrir** pour les téléverser. Vous pouvez aussi saisir un message avant d'appuyer sur **ENTRÉE** pour l'envoyer.

## Aperçu de fichiers

Mattermost possède un aperçu de fichier intégré qui vous permet de visionner les médias, de télécharger les fichiers, et d'en partager les liens. Cliquez sur la miniature d'une pièce jointe pour ouvrir l'aperçu de fichiers.

#### Partage de liens publics

Les liens publics vous permettent de partager vos pièces jointes avec des personnes à l’extérieur de votre équipe Mattermost. Ouvrez l'aperçu de fichier en cliquant sur la miniature d'une pièce jointe, puis cliquez sur **Lien Public**. Le fichier s'ouvrira dans un nouvel onglet, et l'URL peut être partagée avec tout le monde. Les liens publics expirent après un certain temps et deviennent inaccessibles pour des raisons de sécurité.

Si l'option **Lien Public** n'est pas visible dans l'aperçu de fichier, contactez votre administrateur système et demandez-lui d'activer les liens publics dans l'onglet Fichiers de la console système.

#### Télécharger des fichiers

Téléchargez une pièce jointe en cliquant sur l'icône télécharger à côté de la miniature du fichier, ou en ouvrant l'aperçu de fichier et en cliquant sur **Télécharger**.


#### Fichiers média pris en charge

Si vous essayez de prévisualiser un fichier dont le type n'est pas pris en charge, l'aperçu de fichiers ouvrira une icône standard de type média. Les formats pris en charge dépendent fortement de votre navigateur et de votre système d'exploitation, mais les formats suivants sont pris en charge par Mattermost sur la plupart des navigateurs :

Images : BMP, GIF, JPG, JPEG, PNG    
Vidéo : MP4     
Audio : MP3, M4A    

La prévisualisation de documents (PDF, Word, Excel, PowerPoint) n'est pas encore prise en charge.


## Limitation de la taille des fichiers

Framateam supporte un maximum de cinq pièces jointes par message, avec une limite de 5&nbsp;Mo chacune.

<p class="alert alert-warning">
Pour les fichiers plus volumineux, merci d'utiliser <a href="https://framadrop.org">Framadrop</a>.
<p class="alert alert-warning">
