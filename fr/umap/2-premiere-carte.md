# Je crée ma première carte uMap

## Ce que nous allons apprendre

*  distinguer le mode édition du mode consultation
*  identifier les étapes nécessaires pour créer une carte
*  produire une première carte et la diffuser !

## Procédons par étapes

L'objet de notre première carte est simple : positionner un ou plusieurs lieux (domicile, vacances, travail, etc.). Procédons par étapes.

### 1. Le mode édition

Rendez-vous sur le site http://umap.openstreetmap.fr/ et cliquez sur le bouton **Créer une carte**. Apparaît alors sur votre navigateur une carte qui se présente ainsi :

![](images/umap_edition.jpg)

Nous retrouvons à gauche les boutons disponibles lors de la [consultation d'une carte](1-consulter.html).
Plusieurs éléments visibles au-dessus et à droite de la carte sont visibles uniquement lorsque l'on crée ou modifie une carte, c'est-à-dire dans le *mode édition* :

*  le **nom de la carte** en haut à gauche
*  les boutons **Annuler** et **Enregistrer** en haut à droite
*  à droite une série de 3 boutons permettant d'ajouter des éléments à la carte : marqueurs, lignes et polygones
*  en-dessous une série de 6 boutons permettant de configurer la carte

### 2. Nommer la carte

![](images/umap_set_name.png)

![](images/umap_edit_props.png) Une carte doit porter un nom qui renseigne sur ce que représente la carte. Pour définir le nom de la carte, cliquez sur le bouton **Éditer les paramètres**.

Un panneau apparaît sur la droite de la carte, il contient en haut un champ de saisie pour le **nom** de la carte, qui contient le texte ''Carte sans nom'' : placez le curseur dans ce champ, supprimez le texte existant et saisissez le nom de votre carte, par exemple ''Mon domicile''.

Notez que le nom en haut à gauche de la carte est immédiatement modifié. Vous pouvez également saisir un texte plus long dans le champ **description**, qui apparaîtra dans le panneau de légende - nous y reviendrons.

Maintenant sauvegardez la carte avec le bouton **Enregistrer** : un texte en anglais est affiché en haut de la carte, comme celui ci-dessous.

![](images/umap_create_anonymous.png)

Ce texte explique que vous venez de créer une carte **anonyme** et vous donne un lien (une URL) pour pouvoir modifier la carte. En effet la carte que vous avez créée n'est associée à aucun compte, et **uMap** considère que seules les personnes ayant ce *lien secret* peuvent la modifier. Vous devez donc conserver ce lien si vous souhaitez pouvoir modifier la carte. Nous verrons dans [le prochain tutoriel](3-utiliser-un-compte.html) comment créer son catalogue de cartes en utilisant un compte, il n'est alors pas nécessaire de conserver de lien secret.

### 3. Ajouter un marqueur

Commencez par déplacer et zoomer la carte pour visualiser l'endroit précis de votre domicile, lieu de vacances ou de travail.

![](images/umap_edit_marker.png) Cliquez ensuite sur le bouton **Ajouter un marqueur**. Le curseur prend la forme d'un signe ''+'' : déplacez le sur le lieu que vous voulez *marquer* et cliquez avec le bouton gauche de la souris : un *marqueur bleu* et carré est créé à cet endroit et un panneau apparaît à droite.

![](images/umap_marqueur.jpg)
Ce panneau vous permet d'associer un nom et une description au marqueur :

*  le nom sera affiché au survol du marqueur par la souris
*  le nom et la description seront visibles dans une fenêtre dite *popup* qui apparaîtra lors d'un clic sur le marqueur.

Nous verrons plus loin l'utilité des calques, et comment modifier les propriétés du marqueur : forme, couleur, pictogramme, etc.

Répétez l'opération pour ajouter les marqueurs que vous jugez utiles à votre carte.

### 4. Définir l'emprise de la carte

Il est important de définir l'emprise initiale de la carte, c'est-à-dire la partie du planisphère qui sera affichée lors de la consultation de la carte.

Cette emprise doit inclure votre marqueur et permettre de situer la carte. Il convient de trouver un compromis entre un zoom trop éloigné et un zoom trop rapproché. Le bon compromis dépend essentiellement du contenu de la carte : la majorité des marqueurs, lignes et polygones doivent être visibles et utiliser au mieux l'étendue de la carte.

Vous pouvez aussi considérer le public de la carte : une carte expédiée à votre voisin peut être très zoomée, une carte envoyée un correspondant étranger doit permettre de reconnaître le pays où se trouve votre carte.

![](images/umap_edit_extent.png) Pour définir l'emprise, déplacez et zoomez la carte afin d'afficher l'emprise souhaitée puis cliquez sur le bouton **Enregistrer le zoom et le centre actuels**.

<p class="alert alert-info">
uMap enregistre en réalité le centre et le niveau de zoom. Selon la taille de la fenêtre où est affichée la carte, la partie visible pourra varier. Il est utile de prévoir une marge autour du contenu de la carte.
</p>

### 5. Enregistrer la carte

Toute modification de la carte doit être sauvegardée sur le serveur uMap en cliquant sur le bouton **Enregistrer** en haut à droite. Cette opération enregistre toutes les modifications depuis la dernière sauvegarde : vous pouvez donc réaliser plusieurs modifications à la suite puis les enregistrer. A l'inverse le bouton **Annuler** permet de supprimer toutes les modifications depuis la dernière sauvegarde.

Après avoir enregistré les modifications, le bouton Annuler est remplacé par **Désactiver l'édition**. Cela vous permet de quitter le mode édition pour voir la carte en mode consultation. Vous pouvez alors *tester* votre carte : cliquez sur le marqueur pour afficher la popup et vérifier son nom et sa description.

**Félicitations !** Vous avez créé votre première carte uMap. Vous pouvez la diffuser à votre entourage en copiant son URL dans la barre d'adresse du navigateur, ou en copiant son **URL courte** disponible dans le menu **Partager** vu dans le tutoriel [Je consulte une carte uMap](1-consulter.html).

## Faisons le point

Votre première carte est créée, en quelques étapes. L'opération est assez simple, mais le résultat est somme toute assez sommaire. Le [prochain tutoriel](3-utilise-un-compte.html) va nous permettre de créer une jolie carte.



