# Framasite

L'interface de <b class="violet">Frama</b><b class="vert">site</b> permettant de gérer vos sites et vos domaines est développée par Framasoft et disponible sur [notre dépôt Framagit](https://framagit.org/framasoft/Framasite-/framasite), sous licence [AGPL-3.0](https://framagit.org/framasoft/Framasite-/framasite/blob/master/LICENSE).

---

## Pour aller plus loin&nbsp;:

- Utiliser [Framasite](https://frama.site/)
- [Prise en main](prise-en-main.html)
- [Rattacher son nom de domaine à un site ou un wiki](rattachement-nom-de-domaine.html)
- [Contribuez au code](https://framagit.org/framasoft/Framasite-/framasite)
- Un service proposé dans le cadre de la campagne [contributopia](https://contributopia.org/)
- [Soutenir Framasoft](https://soutenir.framasoft.org)
