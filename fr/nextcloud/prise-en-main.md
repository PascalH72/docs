# Prise en main

## Partage par lien

Pour partager un fichier ou un dossier vous devez :

  * cliquer sur l'icône de partage <i class="fa fa-share-alt"></i> (**si vous ne voyez pas l'icône pensez à désactiver votre bloqueur de pub**)
  * cocher **Activer** sur la ligne **Partager par lien public**
  * cliquer sur l'icône <i class="fa fa-ellipsis-h"></i>
  * cliquer sur **Copier l'adresse URL** (le lien est dans votre presse-papier : vous pouvez donc le coller dans un mail ou autres, pour le partager)

![image de partage framadrive](images/drive-partage.png)

En cliquant sur <i class="fa fa-ellipsis-h"></i> vous avez la possibilité de paramétrer les conditions de partage.

## Télécharger toutes mes données

Vous pouvez à tout moment récupérer l'intégralité de vos données (avant de supprimer votre compte par exemple). Pour cela&nbsp;:

  * cochez la case pour sélectionner tous les fichiers et dossiers
  * cliquez sur <i class="fa fa-ellipsis-h" aria-hidden="true"></i> **Actions**
  * cliquez sur **Télécharger** pour télécharger un dossier *zip* de vos données

![gif montrant comment télécharger ses données drive](images/drive_dl_donnes.gif)
