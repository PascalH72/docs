# Prise en mains

## Comment personnaliser…

### la page d'accueil de ma liste ?

Pour cela vous devez vous rendre dans **Admin** (menu latéral gauche) > **Configurer la liste** (menu horizontal) > **Description / Page d'accueil de la liste**.
Pour modifier la page d'accueil de votre liste (celle visible en cliquant sur **Accueil de la liste** dans le menu latéral gauche), vous devez cliquer sur **Editer** devant **Page d'accueil de la liste**

Vous pouvez utiliser la syntaxe HTML.

Par exemple, pour obtenir&nbsp;:

![image d'illustration d'une page d'accueil personnalisée](images/framalistes_personnalisation_accueil.png)

vous devez mettre&nbsp;:

```
Bienvenue sur cette <b>super</b> liste !
<br />
<img src="https://framalistes.org/sympa/d_read/VOTRE-LISTE/NOM-IMAGE.png" width="20%" height="20%"/>
```

  * `<b>` et `</b>` pour afficher en gras
  * `<br />` pour sauter une ligne
  * `<img src="https://framalistes.org/sympa/d_read/VOTRE-LISTE/NOM-IMAGE.png" width="20%" height="20%"/>` pour afficher le logo Framalistes
    * l'image peut venir de votre espace **Documents partagés** (dans le menu latéral gauche) : l'adresse est obtenue en faisant un clic droit sur le nom du fichier puis `Copier l'adresse de l'image`)
    * `width` et `height` étant la largeur et la hauteur que nous réduisons à 20% pour que cela ne soit pas trop grand.



### l'entête des mails

Pour cela vous devez&nbsp;:
  * vous rendre dans **Admin** (menu latéral gauche)
  * puis **Configurer la liste** (menu horizontal)
  * puis **Messages-types**
  * puis dans la section **Ajouts aux messages diffusés**
  * cliquer sur **Editer** devant **Attachement de début de message**

### Le pied-de-page

Il n'est [pas possible de modifier le pied-de-page existant](https://contact.framasoft.org/fr/faq/#date_creation_sondage) mais il est possible d'en ajouter un au-dessus de l'existant. Pour cela vous devez&nbsp;:
  * vous rendre dans **Admin** (menu latéral gauche)
  * puis **Configurer la liste** (menu horizontal)
  * puis **Messages-types**
  * puis dans la section **Ajouts aux messages diffusés**
  * cliquer sur **Editer** devant **Attachement de début de message**

## Rôles et privilèges

### Gestionnaire privilégié

Le premier gestionnaire privilégié défini est la personne qui a demandé la création de la liste. Plus tard, il peut être modifié ou étendu. Il hérite des privilèges de propriétaire (de base) et sont également responsables de la gestion des propriétaires et des modérateurs de la liste eux-mêmes (via l'interface web). Avec le comportement par défaut de Sympa, les propriétaires privilégiés peuvent éditer plus de paramètres de liste que les propriétaires (de base) ne peuvent le faire

### Gestionnaire (simple)

Ils sont responsables de la gestion des membres de la liste, de l'édition de la configuration de la liste et des modèles.

### Modérateur

Les modérateurs sont responsables des messages distribués dans la liste de diffusion (par opposition aux propriétaires qui s'occupent des membres de la liste). Les modérateurs sont actifs si la liste a été configurée comme liste de diffusion modérée. Si aucun modérateur n'est défini pour la liste, les propriétaires de liste hériteront du rôle de modérateur.

### Ajouter un gestionnaire

Si vous êtes [gestionnaire privilégié](#gestionnaire-privilégié) vous pouvez ajouter d'autres gestionnaires (privilégiés ou [simples](#gestionnaire-simple)). Pour cela&nbsp;:

  * cliquez sur **Admin** dans le menu latéral gauche
  * cliquez sur **Les utilisateurs** puis sur **propriétaires**
  * dans la section **Ajout de propriétaires**&nbsp;:
    * [optionnel] cliquez sur **gestionnaire privilégié**
    * indiquez l'adresse mail de la personne dans **adresse email**
    ![image création gestionnaire](images/listes_ajout_gestionnaire.png)

Si la personne est gestionnaire privilégié, une étoile sera affichée à côté de son nom dans la liste&nbsp;:

![image gestionnaire privilégié](images/listes_gestionnaire_priviliegie.png)


## Documents partagés

### Déposer une image dans l'espace partagé

Pour cela vous devez&nbsp;:
  * cliquer sur **Documents partagés** (dans le menu latéral gauche)
  * cliquer sur **Parcourir** sous **Télécharger un fichier**
  * chercher l'image sur votre ordinateur

Les fichiers sont alors accessibles depuis leur URL en faisant un clic droit dessus dans la liste, puis `Copier l'adresse de l'image`.

![image d'une liste des fichiers sur documents partagés de framalistes](images/framalistes_doc_partage.png)
