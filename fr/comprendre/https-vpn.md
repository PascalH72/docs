# Comprendre la connexion sécurisée et les VPN (avec des suppositoires)


## Imaginons : 

J'ai besoin d'acheter une boite de suppositoires (oui, prenons un exemple qui prouve qu'on a tous des choses à cacher, même débiles / minimes).

## Si je suis en connexion http

Je sors de chez moi, je vais à la pharmacie Bidule (tout le monde dans la rue peut voir que j'y vais).

Je fais ma demande au guichet en plein milieu de la pharmacie. Rien n'empêche une personne mal intentionnée de noter tout ce que je dis au pharmacien (ce que je veux, les instructions du pharmacien, même quelle maladie j'ai si je lâche l'info (et sur internet : récupérer mes informations bancaires).

Je repars chez moi avec ma boite de suppos à la main (classe !) : tout le monde sait que j'ai été à la pharmacie et ce que j'ai acheté.

**Je me balade à poil, on sait tout de moi et en plus on peut dévaliser mon compte en banque.**

## Si je suis en connexion https

Idem, tout le monde sait que je vais à la pharmacie MAIS cette fois c'est la pharmacie Machin, qui propose des guichets fermés.

Je suis seul avec le pharmacien, je fais mon achat et je repars avec la boite de suppos dans un beau petit sac opaque (chiffrement) mais avec quand même le nom de la pharmacie dessus.

**On sait que je reviens de la pharmacie mais personne ne sait ce que j'ai dit ou acheté au pharmacien.**

Il faut que je puisse faire confiance au pharmacien. S'il se trouve qu'il va répéter à qui veut l'entendre ce que j'ai acheté (fuite de données), ma boite de suppos n'est plus un secret (malgré mon magnifique sac opaque).

## Si je me connecte via un VPN

Je ne vais pas à la pharmacie, je vais voir une société de coursiers (tout le monde sait que j'utilise cette société du coup), je suis accueilli dans un bureau fermé, je fais ma demande (chiffrement entre moi et le VPN).

Mon coursier sort, va faire des courses pour plusieurs clients, dont moi, et revient (avec la boite de suppos à la main (http) ou dans un sac (https), selon si je lui ai demandé d'aller à la pharmacie Bidule ou la pharmacie Machin).

Toujours dans un bureau fermé, il met mes suppos dans un sac de sa société et me les donne.

**Je rentre chez moi : on sait juste que je suis allé voir ma société de livraison.**

_Si quelqu'un a pisté le coursier, comme il a fait des courses pour plusieurs personnes en même temps, l'observateur ne peut pas déterminer avec certitude quelle demande correspond à quel client._ Cependant si le coursier conserve un registre de ses courses (conservation des logs), ma visite à la pharmacie peut être connue des personnes ayant accès au registre.

Dans cette affaire de suppos, le coursier **doit être de confiance**. Vous ne confierez pas votre santé dans les mains d'un inconnu, trouvé par petites annonces de « coursiers gratuits », qui peut garder vos notes de courses et remplacer votre aspirine par du viagra ? Dans le cas où la pharmacie Bidule donne la boite de suppos à la main (http) au coursier, ce dernier peut voir ce que ce j'ai commandé et même modifier le médicament (ou ajouter de la sauce piquante sur mes suppos, outch !) sans que je m'en rende compte.

## Si je me connecte avec Tor

Tor est une sorte de réseau de coursiers volontaires et engagés sur l'anonymat et la protection des données.

Le principe global est que je vais confier ma demande à plusieurs coursiers, qui vont se succéder jusqu'à la pharmacie, puis me ramèneront ma boite de suppos en suivant le chemin dans le sens inverse.

Pour obtenir ma boîte de suppos de la pharmacie, je commence par récupèrer une liste de coursiers du réseau Tor (appelés "Noeuds"). Pour obtenir mes suppos, je choisis au hasard les coursiers qui vont se relayer.

Je prépare un sac un peu spécial, qui comporte plusieurs couches, comme un oignon. Grâce à ce sac spécial, chaque coursier n'a accès qu'à la couche de l'oignon - et donc à l'information - dont il a besoin.

J'emmène mon sac au premier coursier. Il me reçoit chez lui et je lui passe le sac (connexion chiffrée). Lorsqu'il regarde dans le sac, il apprend seulement l'adresse du coursier suivant. Il se rend chez le deuxième coursier et lui passe le sac (encore une fois, connexion chiffrée). Le second coursier ouvre le sac à son tour pour trouver le nom du coursier suivant. Plusieurs passages de relais se succèdent ainsi jusqu'à arriver au dernier coursier.

Le dernier coursier est un peu spécial car c'est le seul à avoir accès au "coeur" de l'oignon, c'est-à-dire le contenu de ma demande (l'adresse de la pharmacie et le fait que je veux une boite de suppos). Il ouvre le sac, prend connaissance de ma demande et se rend donc à la pharmacie pour récupèrer ma boite de suppos. Selon s'il s'agit de la pharmacie Bidule ou Machin, on l'accueillera dans un bureau fermé et on lui donnera la boite de suppos dans un sac (voir section **connexion http**) ou non (voir section **connexion https**).
Il rentre chez lui, met ma boite de suppos dans mon sac (en incluant le sac de la pharmacie s'il y en a un) et le ramène chez le coursier qui le lui avait apporté.

Chaque coursier de l'aller va se succèder en sens inverse jusqu'à ce que mon sac me revienne.

- Le premier coursier sait : qui je suis et le coursier à qui il doit passer le relais. Il ne connaît pas ma demande.
- Le dernier coursier sait : le contenu de ma demande et le coursier qui le précède. Il ne connaît pas mon identité.
- Chaque autre coursier, dit "intermédiaire" sait : le coursier qui le précéde et celui qui le suit. Il ne connaît ni mon identité ni ma demande.

Quelqu'un qui me suivrait saura seulement l'adresse du premier coursier. Une personne qui surveillerait la pharmacie pourra éventuellement entendre le dernier coursier commander des suppos (si connexion http non sécurisée) et connaître son adresse mais ne pourra pas le relier à moi. Il est fortement recommandé de toujours privilégier une connexion sécurisée (https), même en utilisant Tor, dans le cas où le dernier coursier ne serait finalement pas très honnête (si le "noeud" est mal sécurisé ou compromis (voir l'avertissement de fin de la section **VPN**)).

A noter : utiliser le réseau peut aussi me permettre d'accéder à certaines pharmacies cachées qui ne sont accessibles que par Tor (on appelle cela les services cachés).

## Remarque importante

Si je joins ma carte Vitale à ma note de course (sur Internet : s'identifier avec son compte) ou que le pharmacien reconnaît mon écriture (tracking/pistage web), le pharmacien saura que j'ai fait la commande, même si c'est un coursier qu'il a en face de lui (connexion avec un VPN ou avec Tor). Il est important de garder en tête qu'utiliser un VPN ou Tor ne vous rend pas nécessairement anonyme sur Internet.
