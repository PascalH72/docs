# Astuces

## Je souhaite envoyer des mails en fonction des réponses

Je veux envoyer un mail à une adresse différente en fonction du ou des comités séléctioné :
* je vais dans l'onglet **Courriels**
* je sélectionne le composant
* j'ajoute les adresses mail en fonction du comité
* je sauvegarde (tout en bas)

![composant selections multiples](images/astuces_forms_courriel_comite.png)
![courriel en fonction du composant](images/astuces_forms_courriel_comite2.png)

## Je souhaite recevoir un mail à chaque réponse effectuée sur mon formulaire

Pour recevoir un mail à chaque fois que quelqu'un répond à votre formulaire, vous devez&nbsp;:

  * aller dans l'onglet **Formulaire**
  * cliquer sur **Courriels**
  * cliquer sur le bouton **Ajouter un courriel standard** dans la section **Standard emails (always send)**
  * dans la section **Adresse de courriel du destinataire** vous devez entrer votre adresse mail dans **Personnalisé**
  * vérifier que **Activer envoi** est coché
  * laisser ou modifier le modèle de courriel - le modèle par défaut sera :

    ```
    Soumis le [submission:date:long]
    Soumis par l'utilisateur : [submission:user]
    Les valeurs soumises sont:
    [submission:values]

    Les résultats de cette soumission peuvent être vus ici :
    [submission:url]
    ```

    c'est-à-dire :

    ```
    Soumis le Lundi, décembre 17, 2018 - 11:15
    Soumis par l'utilisateur : Anonyme
    Les valeurs soumises sont:

    Nombre de personnes: 3
    Un message à laisser ?: Je serais probablement en retard !

    Les résultats de cette soumission peuvent être vus ici :
    https://framaforms.org/node/XXXXX/submission/XXXXXXXX
    ```

  * cliquer sur **Enregistrer les paramètres de courriel**

## Je souhaite un système de CAPTCHA

* j'ajoute un champ texte à mon formulaire (en cochant **Requis** dans l'onglet **Validation** du composant)
* dans l'onglet **Validation du formulaire** j'ajoute une règle **Motif**
* je remplis les champs comme sur l'image ci-dessous

![Règle Motif Framaforms](images/astuces_forms_captcha.png)

## Je souhaite un champ « autre » libre

Je voudrais qu'en cliquant sur le choix « autre », les répondant⋅e⋅s puissent mettre un choix libre. Pour cela :

* je créé un composant « Boutons radio » ![composant bouton radio](images/astuces_forms_autre1.png)
* je créé un composant « Champ texte » (ici, avec le titre « Autre ?») ![composant texte autre](images/astuces_forms_autre2.png)
* j'enregistre
* je vais dans « Champs conditionnels »
* je créé la règle : « Si la mascotte est autre alors Autre ? est affiché » (si le champ autre du composant « Boutons radio » est « autre » alors on affiche le composant « Champ texte » - sinon, il ne le sera pas) ![conditions](images/astuces_forms_autre3.png)

Résultats :

![Résultat autre caché](images/astuces_forms_autre4.png)
![Résultat autre visible](images/astuces_forms_autre5.png)

## Je souhaite afficher des images pour les choix

Vous pouvez le faire en y insérant une photo **déjà disponible sur le web** et en utilisant la balise HTML `img`. Par exemple, si vous souhaitez insérer l'image du logo Framasoft (disponible en cliquant-droit sur le logo de la barre de navigation haute et en cliquant sur **Afficher l'image**)  dans votre composant **Boutons radios**, vous devez insérer `<img src="https://framasoft.org/nav/img/logo.png" />` dans un champ.

![image dans un composant boutons radios](images/astuces_forms_images_composant_radios.png)

## Je souhaite visualiser mes résultats dans un Framacalc

Dans Framaforms, vous devez :

  * vous rendre dans l'onglet **Résultats**, puis **Téléchargement**
  * sélectionner `Virgule` dans **Format du texte délimité** pour que ce soit au format `.csv` (`c` étant pour `comma` qui signifie virgule en anglais)
  * cliquer sur **Téléchargement** pour télécharger le fichier sur votre ordinateur

Dans Framacalc :

  * allez dans l'onglet **Presse-papier**
  * cochez **Format CSV**
  * collez le contenu de votre fichier `csv` framaforms dans le cadre
  ![forms dans calc](images/astuces_forms_calc.png)
  * cliquez sur le bouton **Charger le presse-papier de SocialCalc avec ceci**
  * dans l'onglet **Édition**, cliquez sur l'icône **Coller** (ou en faisant `CTRL+v`)

## Je souhaite créer une échelle sémantique différentielle

Pour créer une [échelle sémantique différentielle](https://fr.wikipedia.org/wiki/%C3%89chelle_s%C3%A9mantique_diff%C3%A9rentielle), il est possible d'utiliser le composant **Grille**. Pour ce faire, vous devez&nbsp;:
  * mettre votre question en titre
  * dans l'onglet **Options**, mettre des `l` (la lettre `L` en minuscule) et `|` (`Alt Gr`+`6`) au milieu
  * supprimer des questions dans la zone **Questions**
  * mettre dans la question restante vos valeurs (ici, "froid" et "chaud") : `Froid | Chaud` ; en mettant `|` cela permet de mettre une valeur de chaque côté de la ligne

![forms paramétrage échelle](/images/astuces-forms-echelle.png)

Résultat :

![forms résultat échelle](images/astuces-forms-echelle-resultat.png)

Pour éviter le [bug dans les résultats avec le composant Grille](https://framagit.org/framasoft/framaforms/issues/12), n'oubliez pas de personnaliser les clés. Vous pouvez, par exemple, cliquer sur **Options** > **Saisie manuelle** et ajouter :

```
-3|l
-2|l
-1|l
n||
1|l
2|l
3|l
```
Pour faire l'exemple ci-dessus.

## Je souhaite rendre un champ obligatoire

Si vous souhaitez que les personnes mettent leur nom (pour un formulaire d'inscription, par exemple), vous pouvez rendre le champ **Nom** obligatoire en&nbsp;:

  * ajoutant un composant **Champ texte**
  * lui donnant le **Titre** «&nbsp;Nom&nbsp;»
  * en cliquant sur l'onglet **Validation** (**1**)
  * en cochant **Requis** (**2**)
  * en cliquant sur **Enregistrer** (**3**)

![Image illustrant les étapes](images/astuces_champ_requis.png)

Les personnes qui répondent aux formulaires ne pourront pas le valider sans avoir entré leur nom dans le champ requis.

<div class="alert alert-info">
  Les champs obligatoires sont marqués d’un <b> * </b> rouge.
</div>
