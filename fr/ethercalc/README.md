# Framacalc

Framacalc est un tableur collaboratif en ligne. Vos données sont automatiquement sauvegardées sur internet, et vos amis peuvent collaborer sur le document en même temps. Visualisez tous les changements en temps réel !

Travaillez ensemble sur vos inventaires, vos sondages, vos contenus, vos réunions et bien plus !

Le service repose sur le logiciel libre [Ethercalc](https://github.com/audreyt/ethercalc).

## Fonctionnalités

* Édition collaborative (plusieurs utilisateurs connectés à la même feuille de calcul peuvent modifier simultanément les cellules)
* Nombreuses fonctions disponibles (statistiques, financières, mathématiques, texte, etc.)
* Possibilité de commenter des cellules
* Sauvegarde permanente et automatique
* Graphiques de base (histogramme, lignes, points)
* Export HTML, CSV
* Taille du document : jusqu’à 100 000 lignes

Les documents Framacalc sont faciles à partager (il suffit d’en communiquer l’adresse), à exporter (en HTML, CSV), sont sauvegardés en permanence et automatiquement, et restent toujours accessibles par internet

## Pour aller plus loin&nbsp;:

* [Essayer Framacalc](http://www.framacalc.org/)
* [Les fonctionnalités](fonctionnalites.md)
* [Dégooglisons Internet](https://degooglisons-internet.org)
* [Soutenir Framasoft](https://soutenir.framasoft.org)
