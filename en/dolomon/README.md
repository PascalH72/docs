# Framaclic

[Framaclic](https://framaclic.org) is an online link management service.

By creating an account, you can register web addresses, which will give you another web address to use instead of the original one.

Visits on the new address will be counted, allowing you to get statistics on the number of visits, without spying on anyone.

![ajout dolo](img/addDoloVFVVOK.png)

You can discover the functionalities and uses of Framaclic by consulting our [user case](user-case.md).

---

## See more:

* An [user case](user-case.md)
* Test [Framaclic](https://framaclic.org/)
    * (optional) [WordPress Dolomon's plugin](https://fr.wordpress.org/plugins/dolomon/)
* Install [Dolomon on your own server](https://framacloud.org/fr/cultiver-son-jardin/dolomon.html)
* Contribute [to Dolomon's code](https://framagit.org/luc/dolomon)
