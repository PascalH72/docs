# Getting started

![first interface image](img/interface1.png)

  1. select expiration
  - see your previous drops with this browser
  - link to [expiration delays](https://framadrop.org/delays)
  - click here if you want to delete your file at first download
  - drop zone


![uploaded files image](img/interface2.png)

  1. download file button
  - copy to clipboard button
  - send by email button
  - delete your file button

## Send links by mail

Once your file is in Framadrop, you can send the link by email to the recipients of your choice. To do so, click on **Send all links by email** or on the icon <i class="fa fa-fw fa-envelope"></i> (see 3 in the image above).

You then arrive on the personalization interface where you can:
  * indicate the recipient addresses in the field **Comma-separated email addresses** ; you can put only one address (without commas at the end) or several (separated by commas : `address@mail.com,address2@mail.com,address3@mail.com`)
  * indicate a subject in **Email subject**
  * customize the body of the mail by removing what you do not need

To send:

  * **Send with this server**: send directly from your browser
  * **Send with your own mail software**: send using Thunderbird or Outlook and so

![email interface](img/interface3.png)
